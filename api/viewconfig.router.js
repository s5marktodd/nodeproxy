const Promise = require('bluebird');
const _ = require('lodash');

const fs = Promise.promisifyAll(require('fs'));

const router = require('express').Router();

async function getViewConfig(/*string*/ view) {
  const filename = `${__dirname}/mocks/config/${view}.json`;
  const contents = await fs.readFileAsync(filename).catch(err => {
    console.error(err);
    console.error(`File ${filename} not found. Using default view config.`);
    return '{}';
  });
  return JSON.parse(contents);
}

router.get('/:view', async (req, res) => {
  let view = _.get(req.params, 'view');
  res.setHeader('Content-Type', 'application/json');
  if (view) {
    const viewConfig = await getViewConfig(view);
    res.send(JSON.stringify(viewConfig));
  }
});

module.exports = router;
