const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const router = require('express').Router();
const PRIMARY_FILEPATH = `${__dirname}/mocks/data/validMembers`;

async function _getJsonFile(filepath, ifError) {
  try {
    const contents = await fs.readFileAsync(filepath);
    return JSON.parse(contents);
  } catch (err) {
    console.error(err);
    return ifError;
  }
}
router.use((_req, res, next) => {
  res.setHeader('Content-Type', 'application/json');
  next();
});
router.get('/:memberType', async (req, res) => {
  let path = `${PRIMARY_FILEPATH}/${req.params.memberType}.json`;
  res.json(await _getJsonFile(path, []));
});
module.exports = router;
