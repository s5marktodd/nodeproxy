const _ = require('lodash');
const router = require('express').Router();

function createSimilarStyles(/* list */ styles) {
  _.noop();
  return styles;
}
function addStylestoAssortment(/* list */ styles) {
  _.noop();
  return styles;
}

function _splitArray(array, predicate) {
  let res = [[], []];
  return _.reduce(
    array,
    (acc, curr) => {
      let ind = predicate(curr) ? 0 : 1;
      acc[ind].push(curr);
      return acc;
    },
    res,
  );
}

router.post('/addStyles', (req, res) => {
  let items = req.body;
  let [similar, existing] = _splitArray(
    items,
    i => i.type === 'similar' || i.type === 'design',
  );
  similar = createSimilarStyles(similar);
  addStylestoAssortment(similar.concat(existing));
  res.status(201);
  res.json({
    status: 'Success',
    message: 'Successfully added styles to assortment.',
  });
});

module.exports = router;
