const express = require('express');
const bodyParser = require('body-parser');
const cors = require('./middleware/cors');
const PORT = 3555;
const styleRouter = require('./style.router');
const viewConfigRouter = require('./viewconfig.router');
const assortmentRouter = require('./assortment.router');
const validMembersRouter = require('./validMember.router');

var app = express();
const apiRouter = express.Router({ mergeParams: true });
app.use(cors);
app.use(bodyParser());
app.use('/v2/api', apiRouter);
apiRouter.get('/', (_req, res) => {
  res.send('Found.');
});
apiRouter.use('/style', styleRouter);
apiRouter.use('/config', viewConfigRouter);
apiRouter.use('/assortment', assortmentRouter);
apiRouter.use('/validMembers', validMembersRouter);

app.all('/', (_req, res, _next) => {
  res.setHeader('Content-Type', 'application/json');
  res.send(
    JSON.stringify({
      status: 'Success.',
      message: 'Demo of Version 2 of S5 Stratos api is running.',
    }),
  );
});

app.listen(PORT);
console.info(`Listening on ${PORT}`);
module.exports = {
  port: PORT,
};
