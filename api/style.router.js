const Promise = require('bluebird');
const _ = require('lodash');
const fs = Promise.promisifyAll(require('fs'));
const styleRouter = require('express').Router();

const PRIMARY_FILEPATH = `${__dirname}/mocks/data/style`;
async function _getJsonFile(filepath, ifError) {
  try {
    const contents = await fs.readFileAsync(filepath);
    return JSON.parse(contents);
  } catch (err) {
    console.error(err);
    return ifError;
  }
}
async function getStyleColor(/*string*/ view) {
  const filename = _.isNil(view)
    ? `${PRIMARY_FILEPATH}/stylecolor.json`
    : `${PRIMARY_FILEPATH}/stylecolor.${view}.json`;
  return _getJsonFile(filename, []);
}
async function getStyle(/* string */ view) {
  const filename = _.isNil(view)
    ? `${PRIMARY_FILEPATH}/style.json`
    : `${PRIMARY_FILEPATH}/style.${view}.json`;

  return _getJsonFile(filename, []);
}
async function getStyles(/* string */ view) {
  const filename = _.isNil(view)
    ? `${PRIMARY_FILEPATH}/styles.json`
    : `${PRIMARY_FILEPATH}/styles.${view}.json`;

  return _getJsonFile(filename, []);
}
async function getFloorsets(/* string */ view) {
  const filename = _.isNil(view)
    ? `${PRIMARY_FILEPATH}/stylecolor.floorsets.json`
    : `${PRIMARY_FILEPATH}/stylecolor.${view}.json`;

  return _getJsonFile(filename, []);
}
async function getFloorsetChoices(/* string */ view) {
  const filename = _.isNil(view)
    ? `${PRIMARY_FILEPATH}/stylecolor.store_eligibility.json`
    : `${PRIMARY_FILEPATH}/stylecolor.${view}.json`;

  return _getJsonFile(filename, []);
}
async function getNewStyleColor(/* */) {
  const filename = `${PRIMARY_FILEPATH}/stylecolor.default.json`;

  return _getJsonFile(filename, {});
}
styleRouter.use((_req, res, next) => {
  res.setHeader('Content-Type', 'application/json');
  next();
});
styleRouter.get('/', async (req, res) => {
  let view = _.get(req.headers, 'view');
  res.json(await getStyles(view));
});
styleRouter.get('/:id/floorsets', async (req, res) => {
  let view = _.get(req.headers, 'view');
  res.json(await getFloorsets(view));
});
styleRouter.get('/:id/floorsets/:floorsetid/choices', async (req, res) => {
  let view = _.get(req.headers, 'view');
  res.json(await getFloorsetChoices(view));
});

styleRouter.route('/:id').get(async (req, res) => {
  //return style info
  let view = _.get(req.headers, 'view');
  res.json(await getStyle(view));
});
styleRouter.route('/:id/stylecolor').get(async (req, res) => {
  let view = _.get(req.headers, 'view');
  let contents = await getStyleColor(view);
  res.json(contents);
});
styleRouter.route('/:id/placeholder').get(async (_req, res) => {
  res.json('https://via.placeholder.com/150x225');
});
styleRouter.route('/:id/newStyleColor').get(async (_req, res) => {
  let contents = await getNewStyleColor();
  res.json(contents);
});

module.exports = styleRouter;
