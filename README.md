# Node Proxy for Assortment 2

Mark wrote this to handle API calls on the front end.

## Getting Started

As a result of Auth0, Darwin (Mason/Jetty) cannot run on port 8080 with this, so you must change it. Here, I'll change it to 8082.

### Pre-Installation Setup

1) Navigate to your Darwin project and open jetty.xml (<DarwinDirectory>/web-apps/web/jetty.xml)
2) On line 121, there is a port that should be set to 8080. Change it to 8082 (or anything you want)

### Installation

1) Run `yarn` or `npm install` (yarn preferred)
2) Navigate to proxyserver.js (./njsproxy/proxyserver.js) and change the ports to your ports (Leave caching disabled)   
    `const ASSORTMENT_PORT = 8081;
const DARWIN_PORT = 8082;` for example

## Running the Proxy

Start Darwin (Mason/Jetty), Assortment-UI, your local config server (if you need it), and then run `npm start` and go to localhost:8080! You should be prompted to login if everything is running correctly.