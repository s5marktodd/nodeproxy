var http = require('http'),
  request = require('request'),
  V2_API_PORT = require('../api/index').port,
  yargs = require('yargs').argv,
  CacheClient = require('mongodb').MongoClient,
  shouldCache = require('./utils/shouldCache'),
  targetSettingMock = require('./mocks/targetsetting.EIN-636'),
  {
    ASSORTMENT_URL,
    ASSORTMENT_PORT,
    DARWIN_PORT,
    DARWIN_URL,
    ENABLE_CACHING,
  } = require('./proxyconfig');

// Set these to your ports

let PROXY_HOST;
PROXY_HOST = `http://${DARWIN_URL}:${DARWIN_PORT}`;
// PROXY_HOST = 'http://172.30.0.124:8081';
// PROXY_HOST = 'https://express-qa.s5.network';
var LOCAL_HOST = `http://${ASSORTMENT_URL}:${ASSORTMENT_PORT}`;
var V2_API_HOST = `http://localhost:${V2_API_PORT}`;

var cachecollection;
var cached = false;
var PORT = 8080;

async function parseArgs(args) {
  if (args.cached != null || ENABLE_CACHING == true) {
    console.info('Caching api calls.');
    cached = true;
    await loadDB();
    if (args.clearCache) {
      await cachecollection.drop().catch(_err => {
        //there was no cache to drop
      });
    }
  } else {
    console.info('Not caching api calls.');
    cached = false;
  }
}
async function loadDB() {
  await CacheClient.connect('mongodb://localhost:27017').then(client => {
    var db = client.db('cache_db');
    cachecollection = db.collection('simple_cache');
  });
}
function getPathFromUrl(url) {
  //return url.split(/[?#]/)[0];
  return url.replace(/_dc=\d+&?/, '');
}

function createServ() {
  http
    .createServer(function(req, res) {
      var host = LOCAL_HOST;

      if (/^\/callback\?.*/.test(req.url)) {
        if (req.headers.referer && req.headers.referer.indexOf('login') >= 0) {
          host = PROXY_HOST;
        } else {
          res.writeHead(302, {
            Location: '/',
          });
          res.end();
          return;
        }
      }
      if (/^\/api.*/.test(req.url)) {
        host = PROXY_HOST;
      }
      if (/^\/v2\/api/.test(req.url)) {
        host = V2_API_HOST;
      }
      if (/^\/login.*/.test(req.url)) {
        host = PROXY_HOST;
      }
      if (/^\/logout.*/.test(req.url)) {
        host = PROXY_HOST;
      }
      if (/^\/static\/(?!css|js|media).*/.test(req.url)) {
        host = PROXY_HOST;
      }
      if (/^\/assortment.*/.test(req.url)) {
        host = PROXY_HOST;
      }
      if (/^\/CLEARCACHE.*/.test(req.url)) {
        return cachecollection
          .drop()
          .catch(_err => {
            //there was no cache to drop
          })
          .then(() => {
            res.writeHead(200, 200);
            res.end('Successfully cleared cache');
          });
      }
      if (/api\/targetSetting\/Location.*WP.*/.test(req.url)) {
        res.writeHead(200, 200, {
          'Content-Type': 'application/json',
        });

        res.end(JSON.stringify(targetSettingMock));
        return;
      }
      if (/^\/TOGGLECACHE.*/.test(req.url)) {
        console.info('Toggling cache.');
        cached = !cached;
        res.writeHead(200, 200);
        res.end(`Cache toggle set to: ${cached}.`);
        return;
      }

      var fullUrl = host + req.url;
      var path = getPathFromUrl(req.url),
        cacheFlag = /^\/api\/.*$/.test(path);
      console.log('Using  ' + fullUrl);
      if (cached && cacheFlag) {
        cachecollection.findOne({ path: path }).then(
          cache_item => {
            if (
              cached &&
              cache_item != null &&
              cache_item.body &&
              cache_item.body.buffer
            ) {
              console.info(`Pulling ${path} from cache.`);
              res.writeHead(cache_item.statusCode, cache_item.statusCode);
              res.end(cache_item.body.buffer);
            } else {
              var pr = request(
                fullUrl,
                { encoding: null, rejectUnauthorized: false },
                function(pError, pRes, pBody) {
                  if (pError) {
                    console.error(pError);
                  }
                  if (cached && !pError && cacheFlag && shouldCache(pRes)) {
                    console.info(`Adding ${path} to cache.`);
                    cachecollection.insertOne({
                      path: path,
                      statusCode: pRes.statusCode,
                      headers: pRes.headers,
                      body: pBody,
                    });
                  }
                },
              );

              req.pipe(pr);
              pr.pipe(res);
            }
          },
          () => {
            var pr = request(
              fullUrl,
              { encoding: null, rejectUnauthorized: false },
              function(pError, pRes, pBody) {
                if (pError) {
                  console.error(pError);
                }
                if (cached && !pError && cacheFlag) {
                  console.info(`Adding ${path} to cache.`);
                  cachecollection.collection('simple_cache').insertOne({
                    path: path,
                    statusCode: pRes.statusCode,
                    headers: pRes.headers,
                    body: pBody,
                  });
                }
              },
            );

            req.pipe(pr);
            pr.pipe(res);
          },
        );
        return;
      } else {
        var pr = request(
          fullUrl,
          { encoding: null, rejectUnauthorized: false },
          function(pError, _pRes, _pBody) {
            if (pError) {
              console.error(pError);
            }
          },
        );

        req.pipe(pr);
        pr.pipe(res);
      }
    })
    .listen(PORT);
}
(async function startup() {
  await parseArgs(yargs);
  createServ();
})();
// if (shouldRunStatic) {
//   const app = express();

//   app.use(express.static('../assortment-ui/build'));
//   app.listen(LOCAL_PORT, () =>
//     console.info(`Statically serving on ${LOCAL_PORT}`)
//   );
// }
