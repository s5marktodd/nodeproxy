function shouldCache(req) {
  switch (req.statusCode) {
    case 200:
    case 203:
    case 300:
    case 301:
    case 302:
      return true;
    case 404:
    case 410:
    default:
      return false;
  }
}

module.exports = shouldCache;