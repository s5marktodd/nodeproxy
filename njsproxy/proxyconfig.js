module.exports = {
  DARWIN_URL: '192.168.1.45',
  DARWIN_PORT: 8081,
  ASSORTMENT_URL: 'localhost',
  ASSORTMENT_PORT: 8081,
  ENABLE_CACHING: false,
};
