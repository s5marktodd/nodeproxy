const express = require('express');
const bodyParser = require('body-parser');
const cors = require('../api/middleware/cors');
const working_set = require('./working_set');
const PORT = 3556;

var app = express();
app.use(cors);
app.use(bodyParser());
var apiRouter = express.Router({ mergeParams: true });
app.use('/ws', apiRouter);

apiRouter.all('/', (_req, res, _next) => {
  res.setHeader('Content-Type', 'application/json');
  res.send(
    JSON.stringify({
      status: 'Success.',
      message: 'Demo of Version 2 of S5 Stratos working-set api is running.',
    }),
  );
});
apiRouter.use((req, res, next) => {
  res.setHeader('Content-Type', 'application/json');
  let sid = req.headers.cookie;
  req.headers.session_id = sid;
  next();
});
apiRouter.route('/:tlev/:id')
  .get(() => {
    res.json(
      working_set.get(req.headers.session_id)
    )
  })
  .post((req, res) => {
    working_set.create(req.headers.session_id, req.body);
    res.json(
      ''
    )
  })
  .patch((req, res) => {
    working_set.patch(req.headers.session_id, req.body);
    res.json(
      ''
    )
  })

app.listen(PORT, () => {
  console.info(`Listening on localhost:${PORT}`);
});

module.exports = {
  port: PORT
}