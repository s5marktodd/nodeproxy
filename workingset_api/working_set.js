const { clone, merge, assoc, pick, prop } = require('ramda');
const set_map = {};

function create(sid, new_map) {
  set_map = assoc(sid, clone(new_map));
}

function patch(sid, merge_map) {
  set_map = assoc(sid, merge(merge_map, prop(sid, set_map)));
}

function get(sid) {
  return prop(sid, clone(set_map));
}

module.exports = {
  create,
  patch,
  get
};


// on way to allowing deep merging
// missing a away back
let v = {
  id: 'woah',
  choice: [
    {
      id: 1,
      name: 'description',
      somethingelest: 'd'
    }
  ]
};
let n = {
  id: 'woah',
  choice: [
    {
      id: 1,
      name: 'nah',
      n: [
        {
          id: 3
        }
      ]
    },
    {
      id: 2,
      name: 'hah'
    }
  ]
}
let concatValues;
let indexer = R.indexBy(R.prop('id'));
let nestedIndexer = R.map(i => {
  if (i == null) return i; 
  if (Array.isArray(i)) return indexer(R.map(nestedIndexer, i)); 
  return i
})
let mergedIndexer = R.mergeDeepLeft(nestedIndexer(n), nestedIndexer(v));

console.log(JSON.stringify(mergedIndexer, null, 2));   